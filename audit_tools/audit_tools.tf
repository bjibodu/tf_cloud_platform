module "audit_tools" {
  source = "git::ssh://git@bitbucket.org/bjibodu/tf_module_aws_audit?ref=aws_audit_0.0.1"


  aws_region            = "${data.aws_region.current.name}"
  aws_account_id        = "${data.aws_caller_identity.current.account_id}"
  dry_run               = false
  email_to_domain       = "crowdvision.com"
  email_from            = "bjibodu@crowdvision.co.uk"
  key_age_notify        = 14
  key_age_max           = 90
  user_inactivity_limit = 30
  tag_product           = "${var.tag_product}"
  tag_env               = "${var.tag_env}"
}
