terraform {
  required_version = "0.11.7"

  backend "s3" {
    bucket         = "cv-remote-state"
    key            = "cv-audit-tools.tfstate"
    region         = "eu-west-2"
    encrypt        = true
  }
}
